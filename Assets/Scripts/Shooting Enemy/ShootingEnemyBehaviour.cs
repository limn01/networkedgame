﻿using UnityEngine;
using System.Collections;

public class ShootingEnemyBehaviour : EnemyBehaviour
{
    public Transform player;
    public float desiredDisatance = 20f;

    NavMeshAgent nav;
    float chaseSpeed = 10f;
    //GameObject target;
    EnemyHealth enemyHealth;
    PlayerHealth playerHealth;
    Animator anim;
    
	// Use this for initialization
	void Start ()
    {
        nav = GetComponent<NavMeshAgent>();
        enemyHealth = GetComponent<EnemyHealth>();
        //target = GameObject.FindGameObjectWithTag("Player");
        playerHealth = player.GetComponent<PlayerHealth>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        anim = GetComponentInChildren<Animator>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        HandleMovement();
	}

     public override void HandleMovement()
    {
        float dist = Vector3.Distance(transform.position, player.position);

        if (enemyHealth.currentHealth > 0 && playerHealth.currentHealth > 0) 
        {
            nav.speed = chaseSpeed;
            nav.SetDestination(player.position);
        }

        if (dist <= desiredDisatance)
        {
            nav.stoppingDistance = desiredDisatance;

            if (dist <= 3)
            {
                transform.position = Vector3.MoveTowards(transform.position, player.position, -desiredDisatance);
            }
          
        }
        if (dist > desiredDisatance)
        {
            nav.Resume();
        }
        
    }
}
