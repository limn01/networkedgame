﻿using UnityEngine;
using System.Collections;

public class EnemyShooting : MonoBehaviour
{
    public GameObject bullet;
    public float bulletSpeed = 0.0f;
    public Transform target;
    public Transform gunEnd;
    GameObject player;
    private bool isShooting = false;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

	// Update is called once per frame
	void Update ()
    {
        Handlelook();
        HandleShoot();
	}

    void Handlelook()
    {
        transform.LookAt(player.transform.position + (-player.transform.up * 1.3f));
    }

    void HandleShoot()
    {
        if (!isShooting)
        {
            StartCoroutine(Cooldown(1));
        }
    }

    IEnumerator Cooldown(int wait)
    {
        isShooting = true;

        GameObject bulletClone = Instantiate(bullet, gunEnd.position, Quaternion.identity) as GameObject;
        bulletClone.GetComponent<Rigidbody>().AddForce(transform.forward * bulletSpeed, ForceMode.Impulse);
        yield return new WaitForSeconds(wait);

        isShooting = false;
    }
}
