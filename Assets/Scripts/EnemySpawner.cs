﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemy;
    public GameObject particle;
    

    private GameObject player;
    public GameObject spawner;
    public GameObject spawnTrigger;

    //private Animator anim;


    void Start()
    {
        //anim = GetComponent<Animator>();
        //spawnTrigger = GameObject.FindGameObjectWithTag("SpawnTrigger");
        player = GameObject.FindGameObjectWithTag("Player");
    }
	
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            
            GameObject particleClone = Instantiate(particle, spawner.transform.position, Quaternion.identity) as GameObject;
            Destroy(particleClone, 2);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player)
        {
            //anim.SetTrigger("Door");
            GameObject enemyClone = Instantiate(enemy, spawner.transform.position, Quaternion.identity) as GameObject;
            spawnTrigger.SetActive(false);

            
        }
        else if (enemy == null)
        {
            SceneManager.LoadScene("MainMenu");
        }
    }

    

    
}
