﻿using UnityEngine;
using System.Collections;

public class EnergyBallDamage : MonoBehaviour
{
    public float bulletDamage = 10f;

    GameObject player;
    PlayerHealth playergHealth;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playergHealth = player.GetComponent<PlayerHealth>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            playergHealth.TakeDamage(bulletDamage);
            Destroy(this.gameObject);
        }

        if (other.gameObject.tag == "Environment")
        {
            Destroy(this.gameObject);
        }
    }

    

	
}
