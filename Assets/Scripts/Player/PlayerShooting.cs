﻿using UnityEngine;
using System.Collections;

public class PlayerShooting : MonoBehaviour
{
    public int gunDamage = 1;
    public float fireRate = 0.10f;
    public float hitForce = 100f;
    public float weaponRange = 100f;
    public Transform gunEnd;
    public GameObject muzzleFlash;

    private Camera mainCamera;
    private WaitForSeconds shotDuration = new WaitForSeconds(0.07f);
    private float nextFire;
    private AudioSource gunSound;
    private EnemyHealth enemyHealth;
    private LayerMask shootableMask;
	// Use this for initialization
	void Start ()
    {
        mainCamera = GetComponentInParent<Camera>();
        gunSound = GetComponent<AudioSource>();
        shootableMask = LayerMask.GetMask("Enemy");
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        HandleShooting();
	}

    void HandleShooting()
    {
        if (Input.GetMouseButton(0) && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;

            StartCoroutine(ShotEffect());

            Vector3 rayOrigin = mainCamera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0.0f));

            RaycastHit hit;

            Debug.DrawRay(rayOrigin, mainCamera.transform.forward, Color.green);
            if (Physics.Raycast(rayOrigin, mainCamera.transform.forward, out hit, weaponRange))
            {
                if (hit.transform.tag == "Enemy")
                {
                    Debug.Log("Enemy is not null");
                    hit.collider.GetComponent<EnemyHealth>().TakeDamage(gunDamage, hit.point);
                }

                //if (hit.rigidbody != null)
                //{
                //    hit.rigidbody.AddForce(-hit.normal * hitForce);
                //}
            }
        }
    }

    private IEnumerator ShotEffect()
    {
        gunSound.Play();

        GameObject muzzleClone = Instantiate(muzzleFlash, gunEnd.transform.position, transform.localRotation) as GameObject;

        yield return shotDuration;

        Destroy(muzzleClone);
    }
}
