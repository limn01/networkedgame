﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    

    GameObject player;
    PlayerHealth playerHealth;
    PlayerScript playerScript;
	// Use this for initialization
	void Start ()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = player.GetComponent<PlayerHealth>();
        playerScript = player.GetComponent<PlayerScript>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        GameOverOnDeath();
	}

    public void GameOverOnDeath()
    {
        if (playerHealth.currentHealth <= 0)
        {
            
            SceneManager.LoadScene("MainMenu");
            
        }
    }
}
