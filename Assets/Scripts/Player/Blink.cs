﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Blink : MonoBehaviour
{
    public GameObject ball;

    private Slider energySlider;
    private RaycastHit lastRayCastHit;
    private PlayerHealth playerhealth;

    void Start()
    {
        playerhealth = GetComponent<PlayerHealth>();

        ball.SetActive(false);
    }


    private GameObject GetLookedAtObject()
    {
        Vector3 origin = transform.position;
        Vector3 direction = Camera.main.transform.forward;
        float range = 500;

        if (Physics.Raycast(origin, direction, out lastRayCastHit, range))
        {

            Debug.DrawLine(origin, direction, Color.red);
            return lastRayCastHit.collider.gameObject;
        }
        else
        {
            return null;
        }
    }

    private void TelePortToLookAt()
    {
        transform.position = lastRayCastHit.point + lastRayCastHit.normal * 2;
    }

    void Update()
    {
        if (Input.GetMouseButtonUp(1))
        {
            if(GetLookedAtObject() != null)
            {
                ball.transform.position = lastRayCastHit.point;
                ball.SetActive(true);
                TelePortToLookAt();
            }

        }
       
    }
	
}
