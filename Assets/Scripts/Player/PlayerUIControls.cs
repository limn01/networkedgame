﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerUIControls : MonoBehaviour
{
    public Slider energyBar;
    public Slider healthBar;

    private PlayerHealth playerHealth;
	// Use this for initialization
	void Start ()
    {
        playerHealth = GetComponent<PlayerHealth>();
	}
	
	// Update is called once per frame
	void Update ()
    {

        healthBar.value = playerHealth.currentHealth;
	}
}
