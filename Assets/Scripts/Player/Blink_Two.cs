﻿using UnityEngine;
using System.Collections;


public class Blink_Two : MonoBehaviour
{
    public GameObject ball;
    public Transform cam;
    public ParticleSystem particle;
    
    private PlayerHealth playerHealth;    
    private float range = 50f;
    
    // Use this for initialization
    void Start ()
    {
        
        playerHealth = GetComponent<PlayerHealth>();
        ball.SetActive(false);
        particle.Stop();
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        HandleBlink();  
	}

    void HandleBlink()
    {
        if (Input.GetMouseButton(1))
        {
            RaycastHit hit;
            Debug.DrawRay(cam.position, cam.forward, Color.black);

            if (Physics.Raycast(cam.position, cam.forward, out hit, range))
            {
                ball.transform.position = hit.point;
                ball.SetActive(true);
                
            }
        }

        if (Input.GetMouseButtonUp(1))
        {
            Debug.DrawRay(cam.position, cam.forward, Color.green);
            RaycastHit hit;
           
            if (Physics.Raycast(cam.position, cam.forward, out hit, range))
            {
                transform.position = ball.transform.position;
                playerHealth.currentEnergy -= 20;
                ball.SetActive(false);
                particle.Play();
            }
        }
    }
}
