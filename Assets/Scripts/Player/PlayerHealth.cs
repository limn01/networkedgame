﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public float startingHealth = 100;
    public float currentHealth;
    public float startingEnergy = 100;
    public float currentEnergy;
    public float healthRegenRate = 1;
    public float energyRegenRate = 5;
    public Slider healthBar;
    public Image damageImage;
    public float flashSpeed = 5f;                               
    public Color flashColour = new Color(1f, 0f, 0f, 0.1f);    


    private bool isDead;
    private bool damaged;
    private PlayerScript playerScript;
    private EnemyBehaviour enemyAttack;
    private GameObject enemy;
    GameObject player;
    
    

    void Start()
    {
        
        playerScript = GetComponent<PlayerScript>();
        enemy = GameObject.FindGameObjectWithTag("Enemy");
        player = GameObject.FindGameObjectWithTag("Player");
        
        
        currentHealth = startingHealth;
        currentEnergy = startingEnergy;
    }

    void Update()
    {
        if (damaged)
        {
            // ... set the colour of the damageImage to the flash colour.
            damageImage.color = flashColour;
        }
        // Otherwise...
        else
        {
            // ... transition the colour back to clear.
            damageImage.color = Color.Lerp(damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }

        // Reset the damaged flag.
        damaged = false;

        HandleRegen();
    }

    void HandleRegen()
    {
        if (currentHealth < startingHealth)
        {
            currentHealth += Time.deltaTime * healthRegenRate;
        }
        else
        {
            currentHealth = startingHealth;
        }

        if (currentEnergy < startingEnergy)
        {
            currentEnergy += Time.deltaTime * energyRegenRate;
        }
        else
        {
            currentEnergy = startingEnergy;
        }
        if (currentHealth <= 0)
        {
            currentHealth = 0;
        }

        if (currentEnergy <= 0)
        {
            currentEnergy = 0;
        }
    }

    public void TakeDamage(float amount)
    {
        damaged = true;

        currentHealth -= amount;

        healthBar.value = currentHealth;

        if (currentHealth <= 0 && !isDead)
        {
            Death();
        }
    }

    void Death()
    {
        isDead = true;

        playerScript.enabled = false;
        //player.SetActive(false);
        
        
    }
}
