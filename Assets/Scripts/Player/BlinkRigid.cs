﻿using UnityEngine;
using System.Collections;

public class BlinkRigid : MonoBehaviour
{
    public Transform cam;
    public float speed;
    public GameObject ball;

    private RaycastHit hit;
    private bool attached = false;
    private float momentum = 10f;
    private float step;
    private Rigidbody rb;
    public CharacterController controller;
    

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        controller = GetComponent<CharacterController>();
        ball.SetActive(false);
    }

    void FixedUpdate()
    {
        if (Input.GetMouseButtonDown(1))
        {
            Blinking();
        }

        if (Input.GetMouseButtonUp(1))
        {
           NotBlinking();
        }
        

        if (attached)
        {
            momentum += Time.deltaTime * speed;
            step = momentum * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, hit.point, step);
        }
        if (!attached)
        {
            momentum = 0;
            attached = false;
            rb.isKinematic = false;
        }

        
    }

    void Blinking()
    {
        if (Physics.Raycast(cam.position, cam.forward, out hit))
        {
            
            attached = true;
            rb.isKinematic = true;
            ball.transform.position = hit.point;
            ball.SetActive(true);

            Debug.DrawLine(cam.position, ball.transform.position, Color.blue);

        }
    }

    void NotBlinking()
    {
        attached = false;
        rb.isKinematic = false;
        rb.velocity = transform.forward * momentum;
        ball.SetActive(false);
    }
}
