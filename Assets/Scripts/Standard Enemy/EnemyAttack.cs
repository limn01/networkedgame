﻿using UnityEngine;
using System.Collections;

public class EnemyAttack : MonoBehaviour
{
    public float timeBetweenAttacks = 0.5f;
    public int attackDamage = 10;

    PlayerHealth playerHealth;
    Transform currentTarget;
    EnemyHealth enemyHealth;
    bool playerInRange;
    float timer;
    GameObject player;
    SphereCollider col;
    NavMeshAgent nav;

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        col = GetComponent<SphereCollider>();
        enemyHealth = GetComponent<EnemyHealth>();
        playerHealth = player.GetComponent<PlayerHealth>();
        nav = GetComponent<NavMeshAgent>();
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject == player)
        {
            playerInRange = true;

            Vector3 relDirection = transform.InverseTransformDirection(nav.desiredVelocity);
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject == player)
        {
            playerInRange = false;
        }
    }

    void Update()
    {
        timer += Time.deltaTime;

        if (timer >= timeBetweenAttacks && playerInRange && enemyHealth.currentHealth > 0)
        {
            Attack();
        }
    }

    void Attack()
    {
        timer = 0f;

        if(playerHealth.currentHealth > 0)
        {
            playerHealth.TakeDamage(attackDamage);
        }
    }
}
