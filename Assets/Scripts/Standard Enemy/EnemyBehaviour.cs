﻿using UnityEngine;
using System.Collections;

public class EnemyBehaviour : MonoBehaviour
{
    public int attackDamage = 10;
    public float timeBetweenAttacks = 0.5f;
    public float chaseSpeed = 40f;

    private NavMeshAgent nav;
    private PlayerHealth playerHealth;
    private EnemyHealth enemyHealth;
    private GameObject player;
    private bool playerInRange;
    private float timer;
    private Animator anim;
    private float desiredDistance = 2.3f;
    private float originalSpeed = 0.0f;
    
	// Use this for initialization
	void Awake ()
    {
        nav = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = player.GetComponent<PlayerHealth>();
        enemyHealth = GetComponent<EnemyHealth>();
        anim = GetComponent<Animator>();
        originalSpeed = nav.speed;
    }
	
	// Update is called once per frame
	void Update ()
    {
        timer += Time.deltaTime;

        HandleMovement();

        if (timer >= timeBetweenAttacks && playerInRange && enemyHealth.currentHealth > 0)
        {
            HandleAttacks();
        }
        
	}

    public virtual void HandleMovement()
    {
        float dist = Vector3.Distance(transform.position, player.transform.position);

        if (enemyHealth.currentHealth > 0 && playerHealth.currentHealth > 0)
        {
            anim.SetBool("isRunning", true);
            anim.SetBool("Attack", false);
            nav.speed = chaseSpeed;
            
            nav.SetDestination(player.transform.position);
        }

        if (dist <= desiredDistance)
        {
            nav.stoppingDistance = desiredDistance;
            anim.SetBool("isRunning", false);
            nav.speed = 0.0f;
            anim.SetBool("Attack", true);
        }

        if (dist > desiredDistance)
        {
            nav.speed = originalSpeed;
        }

    }

    void HandleAttacks()
    {
        timer = 0f;

        if (playerHealth.currentHealth > 0)
        {
            playerHealth.TakeDamage(attackDamage);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            playerInRange = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player)
        {
            playerInRange = false;
        }
    }
}
