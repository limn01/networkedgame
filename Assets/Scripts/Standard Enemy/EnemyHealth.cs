﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour
{
    public int startingHealth = 5;
    public int currentHealth;

    public bool damaged;
    private bool isDead;
    private ParticleSystem hitParticle;
    GameObject enemy;
    
	// Use this for initialization
	void Start ()
    {
        hitParticle = GetComponentInChildren<ParticleSystem>();
        
        currentHealth = startingHealth;
	}
	
	// Update is called once per frame
	void Update ()
    {
	    
	}

    public void TakeDamage(int amount, Vector3 hitPoint)
    {
        damaged = true;

        Debug.Log("HIT");

        
        currentHealth -= amount;

        hitParticle.transform.position = hitPoint;

        hitParticle.Play();

        if (currentHealth <= 0)
        {
           Death();
        }

    }

    void Death()
    {
        isDead = true;
        hitParticle.Play();
        Destroy(this.gameObject);
        
    }
}
